import {
  Box,
  Button,
  Center,
  FormControl,
  FormErrorMessage,
  FormHelperText,
  FormLabel,
  Heading,
  Input,
  InputGroup,
  InputRightElement,
  StackDivider,
  VStack,
} from '@chakra-ui/react';
import type { NextPage } from 'next';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { Ilogin } from '../models/login';

const Login: NextPage = () => {
  const [show, setShow] = useState(false);
  const handleClick = () => setShow(!show);

  const {
    handleSubmit,
    register,
    formState: { errors, isSubmitting },
  } = useForm<Ilogin>();

  function onSubmit(values: Ilogin) {
    return new Promise<void>((resolve) => {
      setTimeout(() => {
        alert(JSON.stringify(values, null, 2));
        resolve();
      }, 3000);
    });
  }

  return (
    <Center minH={'100%'}>
      <VStack
        w={'lg'}
        border={1}
        borderColor='gray.300'
        borderStyle='solid'
        borderRadius={16}
        divider={<StackDivider borderColor='gray.200' />}
        spacing={4}
        align='stretch'
        p={8}>
        <Box>
          <Heading> Login </Heading>
        </Box>
        <VStack spacing={4} as='form' onSubmit={handleSubmit(onSubmit)}>
          <FormControl isInvalid={errors.user}>
            <FormLabel htmlFor='user'>usuario</FormLabel>
            <Input
              id='user'
              type='text'
              placeholder='agregar tu usuario'
              {...register('user', {
                required: 'This is required',
                minLength: { value: 4, message: 'Minimum length should be 4' },
              })}
            />
            <FormErrorMessage>{errors.user && errors.user.message}</FormErrorMessage>
          </FormControl>
          <FormControl isInvalid={errors.password}>
            <FormLabel htmlFor='password'>contraseña</FormLabel>
            <InputGroup size='md'>
              <Input
                id='password'
                pr='4.5rem'
                type={show ? 'text' : 'password'}
                placeholder='*****'
                {...register('password', {
                  required: 'This is required',
                  minLength: { value: 4, message: 'Minimum length should be 4' },
                })}
              />
              <InputRightElement width='4.5rem'>
                <Button h='1.75rem' size='sm' onClick={handleClick}>
                  {show ? 'Hide' : 'Show'}
                </Button>
              </InputRightElement>
            </InputGroup>

            <FormErrorMessage>{errors.password && errors.password.message}</FormErrorMessage>
          </FormControl>
          <Box w='100%' mt={6} d='flex' justifyContent='flex-end'>
            <Button colorScheme='teal' isLoading={isSubmitting} type='submit'>
              Iniciar Sesión
            </Button>
          </Box>
        </VStack>
      </VStack>
    </Center>
  );
};

export default Login;
